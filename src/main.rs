use actix_web::{web, App, HttpServer, Responder};

// Ths is where the magic happens
async fn nop() -> impl Responder {
  "{}"
}

#[actix_rt::main]
async fn main() -> std::io::Result<()> {
  HttpServer::new(|| {
    App::new().route("/v1/*", web::get().to(nop))
  })
  .bind("0.0.0.0:5000")?
  .run()
  .await
}

#[cfg(test)]
mod tests {
  use super::*;
  use actix_web::{test, web, App};

  #[actix_rt::test]
  async fn test_nop_get() {
    let mut app = test::init_service(App::new().route("/v1", web::get().to(nop))).await;
    let req = test::TestRequest::get().uri("/v1").to_request();
    let resp = test::call_service(&mut app, req).await;
    assert!(resp.status().is_success());
  }
}
